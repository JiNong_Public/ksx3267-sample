/*

@file sample_sensor_node.ino
@data 2019-08-02

Copyright (c) 2019 JiNong, Inc.
All right reserved.

*/

#include <Modbus.h>
#include <ModbusSerial.h>
#include <SoftwareSerial.h>


/* 
 장비정보 크기 6
 연결제품코드 크기 4
 노드 상태 크기 2
*/
#define processDeviceInfo 6
#define processConnectedProduct 4
#define processNodeStatus 2

/*
 BAUD = 9600, Slave ID = 2, TXPIN =7
*/
#define BAUD 9600
#define ID 2
#define TXPIN 7

/*
 릴레이 1,2,3,4 핀 각각 설정
*/
int relay1 = 2; 
int relay2 = 3; 
int relay3 = 4; 
int relay4 = 5; 

ModbusSerial mb;

void setup() {

/**
 relay1,2,3,4를 각각 OUTPUT으로 설정한다.
 relay1,2,3,4에 LOW 값을 입력한다.
*/
    pinMode (relay1, OUTPUT);
    pinMode (relay2, OUTPUT);
    pinMode (relay3, OUTPUT);
    pinMode (relay4, OUTPUT);
  
    digitalWrite(relay1, LOW);
    digitalWrite(relay2, LOW);
    digitalWrite(relay3, LOW);
    digitalWrite(relay4, LOW);
/*  
 Config Modbus Serial (port, speed, byte format) 
*/  
    mb.config(&Serial, BAUD, TXPIN); 
    mb.setSlaveId(ID);  

/**
 2~7번까지 각각 회사코드, 제품타입(노드:0, 양액기:1), 제품코드, 프로토콜 버전, 연결장비수, 구역수를 출력
*/
    uint16_t devinfo[6] = {1, 2, 2, 101, 4, 0};
        for (int i = 2; i < processDeviceInfo+2; ++i) {
            mb.addHreg(i,devinfo[i-2]);
        }

/** 
 101~102 번까지 장치코드를 출력.
 */
    uint16_t conproduct[4] = {5101,5102,5103,5104};
    int j=0 ;
    for (int i = 101; i < processConnectedProduct+101; ++i) {
        mb.addHreg(i,conproduct[j++]);
    }

/** 
 201번에 opid, 202번에 노드 상태를 출력.
*/
    uint16_t nodesta[2] = {0,0};
    int k=0 ;
    for (int i = 201; i < processNodeStatus+201; ++i) {
        mb.addHreg(i,nodesta[k++]);
    }
    
 /**
  200번대에 구동기 정보 출력, 300번대에 제어명령을 하기 위해 모드버스 공간 확보
 */
    for(int i = 211 ; i < 250 ; i++){
        mb.addHreg(i, 0);
    }

    for(int i = 311 ; i < 350 ; i++){
        mb.addHreg(i, 0);
    }
}     


void loop() {

    mb.task ();
    
    int cmdCode[4] = {311, 321, 331, 341} ;    // cmdCode : 구동기 제어 명령의 명령코드 주소.  
    int num[4];
    int relayvalue[4];                         // relayvalue : 릴레이에 들어갈 값 (LOW, HIGH)

    for ( int i = 0 ; i <4 ; i++) {
        num[i] = ( cmdCode[i] - 301 ) / 10;   
        if (mb.Hreg(cmdCode[i]) == 201 ) {
            relayvalue[num[i]] = LOW;
        }
        else if (mb.Hreg(cmdCode[i]) == 0)  {
            relayvalue[num[i]] = HIGH;
        }
    }

    digitalWrite (relay1,  relayvalue[1]);
    digitalWrite (relay2,  relayvalue[2]);
    digitalWrite (relay3,  relayvalue[3]);
    digitalWrite (relay4,  relayvalue[4]);    


/**
 211번(opid)에 312번의 opid값 추가
 212번(status)에 311번의 명령코드값 추가
*/
    mb.Hreg(211, mb.Hreg(312));
    mb.Hreg(212,mb.Hreg(311));

/**
 위의 코드와 동일
 10의 자리가 바뀌어 각 relay 2,3,4에 해당.
*/
    mb.Hreg(221, mb.Hreg(322));
    mb.Hreg(222,mb.Hreg(321));
      
    mb.Hreg(231, mb.Hreg(332));
    mb.Hreg(232,mb.Hreg(331));
      
    mb.Hreg(241, mb.Hreg(342));
    mb.Hreg(242,mb.Hreg(341));
}
